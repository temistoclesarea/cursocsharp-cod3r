using System;

namespace Encapsulamento
{

    // Não tem relação de herança, mas esta no mesmo projeto
    public class AmigoProximo
    {
        // Como não tem relação de Herança, tem que ter uma Relação de composição
        // Dentro da Classe tem que ter uma instancia(referencia) de SubCelebridade
        public readonly SubCelebridade amiga = new SubCelebridade();

        public void MeusAcessos()
        {
            Console.WriteLine("AmigoProximo...");

            Console.WriteLine(amiga.InfoPublica); // todos acessam
            //Console.WriteLine(amiga.CorDoOlho); // só acessa por herança
            Console.WriteLine(amiga.NumeroCelular); // esta dentro do projeto
            Console.WriteLine(amiga.JeitoDeFalar); // apesar ser protected, tem acesso pelo projeto
            //Console.WriteLine(amiga.SegredoFamilia); // apesar de estar no projeto, só por herança
            //Console.WriteLine(amiga.UsaMuitoPhotoshop); // não tem acesso
            Console.WriteLine();
        }
    }
}