using System;

namespace Encapsulamento
{

    // herda de Subcelebridade e esta no mesmo projeto
    // não precisa criar instancia pois já herda
    public class FilhoReconhecido : SubCelebridade 
    {

        // New para ocultar a classe em SubCelebridade
        public new void MeusAcessos()
        {
            Console.WriteLine("FilhoReconhecido...");

            Console.WriteLine(InfoPublica); // todos acessam
            Console.WriteLine(CorDoOlho); // recebe por herança no mesmo projeto
            Console.WriteLine(NumeroCelular); // esta no mesmo projeto
            Console.WriteLine(JeitoDeFalar); // ou passado por herança ou mesmo projeto
            Console.WriteLine(SegredoFamilia); // transmitodo por herança ou esta no mesmo projeto
            //Console.WriteLine(UsaMuitoPhotoshop); // não tem acesso
            Console.WriteLine();
        }
    }
}