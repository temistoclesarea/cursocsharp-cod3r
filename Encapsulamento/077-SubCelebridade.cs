using System;

namespace Encapsulamento
{
    public class SubCelebridade
    {
        // Acessado por Todos
        public string InfoPublica = "Tenho um instagram!";

        // Transmitido por Herança
        protected string CorDoOlho = "Verde";

        // Acessado do Mesmo projeto (assembly) - somente dentro do projeto
        internal ulong NumeroCelular = 551199999999;

        // Transmitido por Herança OU dentro do mesmo projeto
        protected internal string JeitoDeFalar = "Uso muitas gírias";

        // Acessado pela mesma classe OU transmitido por herança no mesmo projeto (C# >= 7.2)
        private protected string SegredoFamilia = "Bla bla";

        // Private é o padrão, só visivel na propria classe
        bool UsaMuitoPhotoshop = true;

        public void MeusAcessos()
        {
            Console.WriteLine("SubCelebridade..."); // todos atributos são acessiveis

            Console.WriteLine(InfoPublica);
            Console.WriteLine(CorDoOlho);
            Console.WriteLine(NumeroCelular);
            Console.WriteLine(JeitoDeFalar);
            Console.WriteLine(SegredoFamilia);
            Console.WriteLine(UsaMuitoPhotoshop);
            Console.WriteLine();
        }
    }
}