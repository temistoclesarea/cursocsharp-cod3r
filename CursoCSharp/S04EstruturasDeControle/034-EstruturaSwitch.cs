using System;

namespace CursoCSharpCod3rUdemy.S04EstruturasDeControle
{
    public class EstruturaSwitch
    {
        public static void Executar()
        {
            Console.WriteLine("Avalie meu atendimento com uma nota de 1 a 5:");
            int.TryParse(Console.ReadLine(), out int nota);

            switch(nota) {
                case 0:
                    Console.WriteLine("Péssimo");
                    break;
                case 1:
                case 2:
                    Console.WriteLine("Ruim");
                    break;
                case 3:
                    Console.WriteLine("Regular");
                    break;
                case 4:
                    Console.WriteLine("Bom");
                    break;
                case 5: { // chaves não é obrigatorio
                    Console.WriteLine("Ótimo"); // duas sentenças de código
                    Console.WriteLine("Parabéns!");
                    break;
                }
                default:
                    Console.WriteLine("Nota inválida");
                    break;
            }
            // algumas linguagens faz o break automatico
            // se não tivesse o break, sairia executando os itens abaixo em sequencia
            // ele não faz o comportamento executando todos os debaixo

            Console.WriteLine("Obrigado por responder!");
        }
    }
}