using System;

namespace CursoCSharpCod3rUdemy.S04EstruturasDeControle
{
    public class UsandoContinue
    {
        public static void Executar()
        {
            int intervalo = 50;

            Console.WriteLine("Números pares entre 1 e {0}!", intervalo);

            for (int i = 1; i <= intervalo; i++)
            {
                if (i % 2 == 1)
                {
                    continue;
                }

                Console.Write($"{i} ");
            }

            Console.WriteLine();

            // continue pula a interação para a proxima linha
            // causa a interrupção prematura daquela repetição

            // forma mais eficiente de mostrar os números pares
            for (int j = 2; j <= intervalo; j += 2)
            {
                Console.Write($"{j} ");
            }
        }
    }
}