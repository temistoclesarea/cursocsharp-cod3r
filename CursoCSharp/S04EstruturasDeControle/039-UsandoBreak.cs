using System;

namespace CursoCSharpCod3rUdemy.S04EstruturasDeControle
{
    public class UsandoBreak
    {
        public static void Executar()
        {
            // break não causa efeito na estrutura if
            // causa impacto na estruturas de controle

            Random random = new Random();
            int numero = random.Next(1, 51);

            Console.WriteLine("O número que queremos é {0}.", numero);

            for (int i = 1; i <= 50; i++)
            {
                Console.Write("{0} é o número que queremos? ", i);
                if (i == numero)
                {
                    Console.WriteLine("Sim!");
                    break; // não afeta o if e sim o bloco for
                } else
                {
                    Console.WriteLine("Não!");
                }
            }

            Console.WriteLine("Fim!");

            // o break ou esta associado a um laço switch ou a um laço de repetição


        }
    }
}