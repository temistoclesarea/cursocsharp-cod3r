using System;

namespace CursoCSharpCod3rUdemy.S04EstruturasDeControle
{
    public class EstruturaDoWhile
    {
        public static void Executar()
        {
            string entrada;

            // sempre é executa pelo menos vez
            do {
                Console.WriteLine("Qual o seu nome?");
                entrada = Console.ReadLine();

                Console.WriteLine("Seja bem-vindo {0}", entrada);
                Console.WriteLine("Deseja continuar? (S/N)");
                entrada = Console.ReadLine();
            } while (entrada.ToLower() == "s");
            // só vai testar apos a primeira execução
        }
    }
}