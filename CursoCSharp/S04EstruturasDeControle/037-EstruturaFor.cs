using System;

namespace CursoCSharpCod3rUdemy.S04EstruturasDeControle
{
    public class EstruturaFor
    {
        public static void Executar()
        {
            /* int i = 1;

            while (i <= 10)
            {
                Console.WriteLine($"O valor de i é {i}.");
                i++;
            } */

            // exemplo básico de for
            // laço de repetição mais apropriado para uma quantidade determinada de vezes
            /* for (int j = 1; j <= 10; j++)
            {
                Console.WriteLine($"O valor de j é {j}.");
            } */

            double somatorio = 0;
            string entrada;

            Console.Write("Informe o tamanho da turma: ");
            entrada = Console.ReadLine();
            int.TryParse(entrada, out int tamanhoTurma);

            for (int i = 1; i <= tamanhoTurma; i++)
            {
                Console.Write("Informe a nota do aluno {0}: ", i);
                entrada = Console.ReadLine();
                double.TryParse(entrada, out double notaAtual);

                somatorio += notaAtual;
            }

            double media = tamanhoTurma > 0 ? somatorio / tamanhoTurma : 0;
            Console.WriteLine("Media da turma: {0}: ", media);


            // não é obrigatorio ter inicialização da variavel
            // não é obrigatorio ter o incremento
            // exemplo: for (; media > 9; ){}

            // use o for para o que ele foi feito e projetado
            // use o while para uma quantidade indeterminada de vezes
            // durante a programação poderemos encontrar a melhor estrutura que se adequa ao que queremos utilizar no momento

            // a estrutura for da para ser utilizada tambem de forma contrario, decrementando valores, é flexivel nesse sentido
            
        }
    }
}