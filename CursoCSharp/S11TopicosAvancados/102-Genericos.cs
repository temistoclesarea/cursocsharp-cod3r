using System;
using System.Collections.Generic;
using CursoCSharpCod3rUdemy.S05ClassesEMetodos;

namespace CursoCSharpCod3rUdemy.S11TopicosAvancados
{   
    // notação do genericos para amarrar que tipo é esse que vai ser colocado dentro da caixa
    // T não é letra magica, pode ter mais de um tipo generico
    public class Caixa<T> 
    {
        // onde tem o tipo T, ele vai substituindo em toda classe onde tem atribuido esse tipo na classe
        T valorPrivado; 

        public T Coisa
        { get; set; }

        // T é algo que vai ser substituido depois
        public Caixa(T coisa)
        {
            Coisa = coisa;
            valorPrivado = coisa;
        }

        public T metodoGenerico(T valor)
        {
            return new Random().Next(0, 2) == 0 ? Coisa : valor;
        }

        public T GetValor()
        {
            return valorPrivado;
        }
    }
    // T esta totalmente aberto
    // T é definido em duas situações
    // quando for por herança
    // quando for instanciado

    class CaixaInt : Caixa<int>
    {
        public CaixaInt() : base(0) {}
    }

    class CaixaProduto : Caixa<Produto>
    {
        public CaixaProduto() : base(new Produto()) { }
    }

    public class Genericos
    {
        public static void Executar() 
        {
            var caixa1 = new Caixa<int>(1000);

            Console.WriteLine(caixa1.metodoGenerico(33));
            Console.WriteLine(caixa1.Coisa.GetType());

            var caixa2 = new Caixa<string>("Construtor");
            Console.WriteLine(caixa2.metodoGenerico("Método"));
            Console.WriteLine(caixa2.Coisa.GetType());

            CaixaProduto caixa3 = new CaixaProduto();
            Console.WriteLine(caixa3.Coisa.GetType().Name);
        }
    }
}
