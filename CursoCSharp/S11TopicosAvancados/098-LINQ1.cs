using System;
using System.Collections.Generic;
using System.Linq;

namespace CursoCSharpCod3rUdemy.S11TopicosAvancados
{   
    public class Aluno
    {
        public string Nome;
        public int Idade;
        public double Nota;
    }

    public class LINQ1
    {
        public static void Executar() 
        {
            var alunos = new List<Aluno> {
                new Aluno() {Nome = "Pedro", Idade = 24, Nota = 8.0 },
                new Aluno() {Nome = "Andre", Idade = 21, Nota = 4.3 },
                new Aluno() {Nome = "Ana", Idade = 25, Nota = 9.5 },
                new Aluno() {Nome = "Jorge", Idade = 20, Nota = 8.5 },
                new Aluno() {Nome = "Ana", Idade = 21, Nota = 7.7 },
                new Aluno() {Nome = "Julia", Idade = 22, Nota = 7.5 },
                new Aluno() {Nome = "Marcio", Idade = 18, Nota = 6.8 },
            };

            Console.WriteLine("== Aprovados ==============");
            // passa uma função como parametro e retorna verdadeiro ou falso
            // recebe um Lambda
            // com unica linha consegue criar um filtro
            // where em C# em outras linguagens é filter (pq filtra)
            var aprovados = alunos.Where(a => a.Nota >= 7)
                .OrderBy(a => -a.Nota); // negativo inverte a ordenação

            foreach (var aluno in aprovados) {
                Console.WriteLine(aluno.Nome + " : " + aluno.Nota);
            }

            Console.WriteLine("\n\n== Chamada ==============");
            // transformar uma lista de objeto alunos em uma lista de string de alunos
            // em outras linguagens ela se chama Map
            // Select converte em uma lista do tipo string
            var chamada = alunos.OrderBy(a => a.Nome).Select(a => a.Nome);

            foreach (var aluno in chamada) {
                Console.WriteLine(aluno); // agora temos só a string de alunos
            }

            Console.WriteLine("\n\n== Chamada (por Idade) ==============");
            // Linq - Linguage Integration Query
            // parece SQL dentro do C#
            // virou lista de string porque usou o select
            var alunosAprovados = 
                from aluno in alunos
                where aluno.Nota >= 7
                orderby aluno.Idade
                select aluno.Nome;

            foreach (var aluno in alunosAprovados) {
                Console.WriteLine(aluno); 
            }

        }
    }
}
