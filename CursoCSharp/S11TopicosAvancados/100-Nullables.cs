using System;
using System.Collections.Generic;

namespace CursoCSharpCod3rUdemy.S11TopicosAvancados
{   
    public class Nullables
    {
        static int num4; // variaveis na class são inicializadas com um valor padrão de seu tipo
        public static void Executar() 
        {
            // com nullables é possivel atribuir nulo a valores boleanos ou inteiros
            Nullable<int> num1 = null;

            int? num2 = null;

            // int num3 = null; // não é possivel atribuir nulo a este tipo

            //int num3;
            //Console.WriteLine(num3); // dessa forma você não pode usar uma variavel que não foi inicializada

            Console.WriteLine(num4);

            if (num1.HasValue) {
                Console.WriteLine("Valor de num: {0}", num1);
            } else {
                Console.WriteLine("A variavel não possui valor");
            }

            // cada código é uma estrategia de uso
            // setar um valor nullable em um inteiro como valor padrão
            int valor = num1 ?? 1000; // valor padrão se for nulo é 1000
            Console.WriteLine(valor);

            bool? booleana = null;
            bool resultado = booleana.GetValueOrDefault(); 
            //retorna o valor, se não tiver o padrão ou nulo, retorna falso
            Console.WriteLine(resultado);

            try {
                // aqui tem navegação segura, tem que ter cuidado para testar
                //int x = num1.Value;
                //int y = num2.Value;
                // pode substituir por esse, dependendo do caso
                int x = num1.GetValueOrDefault();
                int y = num2.GetValueOrDefault();
                Console.WriteLine(x + y);
            } catch (Exception ex) {
                Console.WriteLine(ex.Message); // precisa ter um valor para ser usado
            }

            // tem que ter cuidado para tratar eventuais problemas no tipo de erro retornado
            // ideal é que trabalhe o minimo possivel com valores nulos

        }
    }
}
