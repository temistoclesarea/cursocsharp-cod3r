using System;
using System.Collections.Generic;

namespace CursoCSharpCod3rUdemy.S11TopicosAvancados
{   
    public class Dynamics
    {
        public static void Executar() 
        {
            // C# é fortemente tipada, 
            // mas é possivel criar um tipo dinamico

            // com este tipo, a variavel vai ficar mais parecida com linguagens
            // fracamente tipadas, como javascript
            // tem que ter cuidado com variaveis dinamicas
            // ele só encontra erros em tempo de execução
            // normalmente os erros são encontrados em tempo de compilação
            dynamic meuObjeto = "teste"; 

            Console.WriteLine(meuObjeto);

            // string foi substituido para int
            meuObjeto = 3;
            meuObjeto++;

            Console.WriteLine(meuObjeto);

            // Objeto dinamico
            // parecido com javascript, que é regra
            // no C# é uma exceção
            dynamic aluno = new System.Dynamic.ExpandoObject();
            aluno.nome = "Maria Julia";
            aluno.nota = 8.9;
            aluno.idade = 24;

            Console.WriteLine($"{aluno.nome} {aluno.nota} {aluno.idade}");
            // não deve ser usando como forma padrão de se trabalhar
            // é para ser usado em situações mais especificas
            // C# da essa flexibilidade de uso

            // não tente pegar formas de programar em outras linguagens porque muitas vezes o paradigma da linguagens vai tormar muito mais dificil se eu for por um caminho de pegar o costumes de outra linguagens e colocar no C#. 
            // Ex: Programar em Java e tentar aplicar o que fiz em java e colocar no javascript.
            
        }
    }
}
