using System;
using System.Collections.Generic;
using System.Linq;

namespace CursoCSharpCod3rUdemy.S11TopicosAvancados
{   
    public class LINQ2
    {
        public static void Executar() 
        {
            var alunos = new List<Aluno> {
                new Aluno() {Nome = "Pedro", Idade = 24, Nota = 8.0 },
                new Aluno() {Nome = "Andre", Idade = 21, Nota = 4.3 },
                new Aluno() {Nome = "Ana", Idade = 25, Nota = 9.5 },
                new Aluno() {Nome = "Jorge", Idade = 20, Nota = 8.5 },
                new Aluno() {Nome = "Ana", Idade = 21, Nota = 7.7 },
                new Aluno() {Nome = "Julia", Idade = 22, Nota = 7.5 },
                new Aluno() {Nome = "Marcio", Idade = 18, Nota = 6.8 },
            };

            // gera exceção se não existir
            var pedro = alunos.Single(aluno => aluno.Nome.Equals("Pedro"));
            Console.WriteLine($"{pedro.Nome} {pedro.Nota}");

            var fulano = alunos.SingleOrDefault(
                aluno => aluno.Nome.Equals("Fulano"));

            if (fulano == null) {
                Console.WriteLine("Aluno Inexistente!");
            }

            // gera exceção se não existir
            var ana = alunos.First(aluno => aluno.Nome.Equals("Ana"));
            Console.WriteLine(ana.Nota);

            var sicrano = 
                alunos.FirstOrDefault(aluno => aluno.Nome.Equals("Sicrano"));
            
            if (sicrano == null) {
                Console.WriteLine("Aluno Inexistente!");
            }

            var outraAna = 
                alunos.LastOrDefault(aluno => aluno.Nome.Equals("Ana"));

            Console.WriteLine(outraAna.Nota);

            // útil para paginação
            // pegue 3 alunos pulando 1 aluno
            var exemploSkip = alunos.Skip(1).Take(3);

            foreach(var item in exemploSkip) {
                Console.WriteLine(item.Nome);
            }

            // maior numero
            var maiorNota = alunos.Max(aluno => aluno.Nota);
            Console.WriteLine(maiorNota);

            var menorNota = alunos.Min(aluno => aluno.Nota);
            Console.WriteLine(menorNota);

            var somatorioNotas = alunos.Sum(aluno => aluno.Nota);
            Console.WriteLine(somatorioNotas);

            // calcula a media de todos os alunos na lista
            var mediaDaTurma = alunos.Average(aluno => aluno.Nota);
            Console.WriteLine(mediaDaTurma);

            // media dos alunos aprovados
            var mediaDaTurma2 = alunos.Where(a => a.Nota >= 7)
                .Average(aluno => aluno.Nota);
            Console.WriteLine(mediaDaTurma2);

            // trabalhar de forma declarativa/funcional, não precisa descrever cada detalhe, deixa a linguagem trabalhar para você 

            // trabalhar de forma imperativa é como descrever passo a passo como o programa deve se comportar, dizendo os passos

            // Muito bom o C# permitir trabalhar com as funções do linq
        }
    }
}
