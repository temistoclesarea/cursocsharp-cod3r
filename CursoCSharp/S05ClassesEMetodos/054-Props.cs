using System;

namespace CursoCSharpCod3rUdemy.S05ClassesEMetodos
{
    public class CarroOpcional 
    {
        double desconto = 0.1;
        string nome;
        public string Nome
        {
            get {
                return "Opcional: " + nome;
            }
            set {
                nome = value; //value é palavra reservado para atribuir
            }
        }

        // Propriedades autoimplementadas
        public double Preco { get; set; } // pode ser implementado depois

        // Somente leitura
        public double PrecoComDesconto { // não pode ser atribuido pois set não foi implementado
            get => Preco - (desconto * Preco); // Lambda
            // get {
            //     return Preco - (desconto * Preco);
            // }
        }

        public CarroOpcional()
        {}

        public CarroOpcional(string nome, double preco)
        {
            Nome = nome;
            Preco = preco;
        }
    }
    public class Props
    {
        public static void Executar() 
        {
            var op1 = new CarroOpcional("Ar condicionado", 3499.9);

            // op1.PrecoComDesconto = 3000; // somente leitura, não funciona

            var op2 = new CarroOpcional();
            op2.Nome = "Direção Elétrica";
            op2.Preco = 2349.9;

            Console.WriteLine(op1.Nome);
            Console.WriteLine(op1.Preco);
            Console.WriteLine(op1.PrecoComDesconto);

            Console.WriteLine(op2.Nome);
            Console.WriteLine(op2.Preco);
            Console.WriteLine(op2.PrecoComDesconto);
        }
    }
}
