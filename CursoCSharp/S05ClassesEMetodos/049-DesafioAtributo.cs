using System;

namespace CursoCSharpCod3rUdemy.S05ClassesEMetodos
{
    public class DesafioAtributo
    {
        int a = 10;
        public static void Executar() // método estatico não acessa atributos de instancia
        {
            // Acessar variável 'a' dentro do método Executar!
            //Console.WriteLine(a);

            DesafioAtributo desafio = new DesafioAtributo();
            Console.WriteLine(desafio.a);
        }
    }
}
