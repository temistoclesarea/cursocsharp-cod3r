using System;

//se estiver dentro do namespace, não precisa utilizar o use para chamar pessoa
namespace CursoCSharpCod3rUdemy.S05ClassesEMetodos
{
    public class Membros
    {
        public static void Executar()
        {
            Pessoa sicrano = new Pessoa(); //novo objeto do tipo pessoa
            sicrano.Nome = "Temis";
            sicrano.Idade = 38;

            //Console.WriteLine($"{sicrano.Nome} tem {sicrano.Idade} anos.");

            sicrano.ApresentarNoConsole(); //mais amarrado a só mostrar no console
            sicrano.Zerar();
            sicrano.ApresentarNoConsole();

            var fulano = new Pessoa(); //inferencia no tipo convertido pra pessoa
            fulano.Nome = "Tali";
            fulano.Idade = 30;

            var apresetacaoDoFulano = fulano.Apresentar(); //pode tratar a resposta que eu quero
            Console.WriteLine(apresetacaoDoFulano.Length);
            Console.WriteLine(apresetacaoDoFulano);

        }
    }
}
