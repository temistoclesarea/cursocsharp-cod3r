using System;

namespace CursoCSharpCod3rUdemy.S05ClassesEMetodos
{
    public enum Genero 
    {
        Acao, Aventura, Terror, Animacao, Comedia
    };

    public class Filme
    {
        public string Titulo;
        public Genero GeneroDoFilme;
    }
    public class ExemploEnums
    {
        public static void Executar() 
        {
            int id = (int)Genero.Animacao;
            Console.WriteLine(id);

            var filmeParaFamilia = new Filme();
            filmeParaFamilia.Titulo = "Sharktemis 20";
            filmeParaFamilia.GeneroDoFilme = Genero.Comedia;

            Console.WriteLine("{0} é {1}!", filmeParaFamilia.Titulo,
                filmeParaFamilia.GeneroDoFilme);
        }
    }
}
