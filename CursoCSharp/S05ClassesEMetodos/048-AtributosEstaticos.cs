using System;

namespace CursoCSharpCod3rUdemy.S05ClassesEMetodos
{
    public class Produto 
    {
        public string Nome;
        public double Preco;
        public static double Desconto = 0.1;

        public Produto(string nome, double preco)
        {
            Nome = nome;
            Preco = preco;
        }

        public Produto()
        {}

        public double CalcularDesconto()
        {
            return Preco - Preco * Desconto;
        }
    }

    public class AtributosEstaticos
    {
        public static void Executar()
        {
            var produto1 = new Produto("Caneta", 3.3);

            var produto2 = new Produto() {
                Nome = "Borracha",
                Preco = 5.3
            };

            //Desconto default 10%
            Console.WriteLine("Preço com desconto 1: {0}", 
                produto1.CalcularDesconto());
            Console.WriteLine("Preço com desconto 2: {0}", 
                produto2.CalcularDesconto());

            //Desconto de 50%
            Produto.Desconto = 0.8;

            Console.WriteLine("Preço com desconto 1: {0}", 
                produto1.CalcularDesconto());
            Console.WriteLine("Preço com desconto 2: {0}", 
                produto2.CalcularDesconto());

            //Desconto de 2%
            Produto.Desconto = 0.02;

            Console.WriteLine("Preço com desconto 1: {0}", 
                produto1.CalcularDesconto());
            Console.WriteLine("Preço com desconto 2: {0}", 
                produto2.CalcularDesconto());
        }
    }
}
