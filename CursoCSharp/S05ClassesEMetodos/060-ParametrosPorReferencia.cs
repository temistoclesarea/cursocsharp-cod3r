using System;

namespace CursoCSharpCod3rUdemy.S05ClassesEMetodos
{   
    public class ParametrosPorReferencia
    {
        public static void AlterarRef(ref int numero)
        {
            numero = numero + 1000;
        }

        public static void AlterarOut(out int numero1, out int numero2)
        {
            numero1 = 1;
            numero2 = 2;
            numero1 = numero1 + 15;
            numero2 = numero2 + 30;
        }
        public static void Executar() 
        {
            int a = 3;
            // usou ref em dois locais, torna explicito que o valor esta por referencia
            AlterarRef(ref a);
            Console.WriteLine(a);

            //int b = 2; // já esta sendo inicializada
            //as variaveis são tratadas nos métodos como funções e podem ser utilizadas externamente
            AlterarOut(out int b, out int c);
            Console.WriteLine($"{b} {c}");
        }
    }
}
