using System;

namespace CursoCSharpCod3rUdemy.S05ClassesEMetodos
{
    public class Cliente
    {
        public string Nome;
        // precisa de um valor inicial
        // DateTime não pode ser const
        // const é em tempo de compilação
        //const DateTime Nascimento;

        // readonly é avaliado em tempo de execução
        // a primeira vez setado, não pode ser mudado no construtor
        public readonly DateTime Nascimento;
        const int x = 1;
        readonly int y = 10;

        public Cliente(string nome, DateTime nascimento) 
        {
            Nome = nome;
            Nascimento = nascimento;
            //Nascimento = new DateTime(2020, 10, 10);
        }

        public string GetDataDeNascimento()
        {
            return String.Format("{0:D2}/{1:D2}/{2}", 
                Nascimento.Day, Nascimento.Month, Nascimento.Year);
        }
    }

    public class Readonly
    {
        public static void Executar() 
        {
            var novoCliente = new Cliente("Ana Silva", 
                new DateTime(1987, 5, 22));
            Console.WriteLine(novoCliente.Nome);
            Console.WriteLine(novoCliente.GetDataDeNascimento());

            // somente leitura e não pode mais ser alterado
            //novoCliente.Nascimento = new DateTime(2020, 10, 10);
        }
    }
}
