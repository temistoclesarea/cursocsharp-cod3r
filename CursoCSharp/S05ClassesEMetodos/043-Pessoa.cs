using System;

namespace CursoCSharpCod3rUdemy.S05ClassesEMetodos
{
    public class Pessoa
    {
        //atributos dentro de metodos começa com letra minuscula, dentro de class, letra maiuscula, PascalCase
        public string Nome; //string tem valor padrão null, object tb é null
        public int Idade;//int tem valor padrão 0;

        public string Apresentar()
        {
            if(Nome.Length > 0 || Idade > 0) {
            return string.Format(
                $"Olá! Me chamo {Nome} e tenho {Idade} anos.");
            } else {
                return "Nome ou Idade não cadastrados";
            }
        }

        public void ApresentarNoConsole() //não vale a pena usar dessa forma a lógica
        {
            Console.WriteLine(Apresentar());
        }

        public void Zerar()
        {
            Nome = "";
            Idade = 0;
        }
    }
}
