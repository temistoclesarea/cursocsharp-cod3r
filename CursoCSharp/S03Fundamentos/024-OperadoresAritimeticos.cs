using System;

namespace CursoCSharpCod3rUdemy.S03Fundamentos
{
    public class OperadoresAritimeticos
    {
        public static void Executar()
        {
            // Preço Desconto
            var preco = 1000.0;
            var imposto = 355;
            var desconto = 0.1;

            double total = preco + imposto;
            var totalComDesconto = total - total * desconto;
            System.Console.WriteLine("O preço final é {0}", totalComDesconto);

            // IMC
            double peso = 86.5;
            double altura = 1.75;
            //double imc = peso / (altura * altura);
            double imc = peso / Math.Pow(altura, 2); //Pow calcula a potenciação
            Console.WriteLine($"IMC é {imc}.");

            // Número Par/Impar
            int par = 26;
            int impar = 55;
            System.Console.WriteLine("{0}/2 tem resto {1}", par, par % 2);
            System.Console.WriteLine("{0}/2 tem resto {1}", impar, impar % 2);
        }
    }
}