using System;

namespace CursoCSharpCod3rUdemy.S03Fundamentos
{
    public class OperadorTernario
    {
        public static void Executar()
        {
            var nota = 9.0;
            bool bomComportamento = true;

            string resultado = nota >= 7.0 && bomComportamento ? "Aprovado" : "Reprovado";

            System.Console.WriteLine(resultado);
        }
    }
}
