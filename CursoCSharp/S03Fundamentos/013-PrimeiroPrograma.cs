using System;
using System.Collections.Generic;
using System.Text;

// não é obrigatorio o nome do namespace ser igual ao nome da pasta, mas é bom deixar assim para uma melhor organização
namespace CursoCSharpCod3rUdemy.S03Fundamentos
{
    
    public class PrimeiroPrograma
    {
        public static void Executar() // Main é a porta de entrada de um programa C#
        {
            System.Console.Write("Primeiro ");
            Console.WriteLine("programa.");
            Console.WriteLine("Terminou!");
        }
    }
}
