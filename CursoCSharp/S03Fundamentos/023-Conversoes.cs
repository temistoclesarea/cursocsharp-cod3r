using System;

namespace CursoCSharpCod3rUdemy.S03Fundamentos
{
    public class Conversoes
    {
        public static void Executar()
        {
            int inteiro = 10;
            double quebrado;
            // conversão implicita - é uma conversão direta sem perca de informações
            quebrado = inteiro;
            System.Console.WriteLine(quebrado);

            double nota = 9.7;
            // conversão explicita - é uma conversão que pode haver perdas de informação, casting
            int notaTruncada = (int) nota;
            System.Console.WriteLine("Nota truncada: {0}", notaTruncada);

            // conversão entre string e inteiro utilizando tryParse
            System.Console.Write("Digite sua idade: ");
            string idadeString = Console.ReadLine();
            int idadeInteiro = int.Parse(idadeString);
            System.Console.WriteLine("Idade inserida: {0}", idadeInteiro);

            idadeInteiro = Convert.ToInt32(idadeString);
            System.Console.WriteLine("Resultado: {0}", idadeInteiro);

            Console.Write("Digite o primeiro número: ");
            string palavra = Console.ReadLine();
            int numero1;
            int.TryParse(palavra, out numero1);
            Console.WriteLine("Resultado 1: {0}", numero1);

            Console.Write("Digite o segundo número: ");
            int.TryParse(Console.ReadLine(), out int numero2);
            Console.WriteLine("Resultado 2: {0}", numero2);


        }
    }
}
