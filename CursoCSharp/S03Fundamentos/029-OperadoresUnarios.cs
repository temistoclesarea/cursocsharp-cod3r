using System;

namespace CursoCSharpCod3rUdemy.S03Fundamentos
{
    public class OperadoresUnarios
    {
        public static void Executar()
        {
            var valorNegativo = -5;
            var numero1 = 2;
            var numero2 = 3;
            var booleano = true;

            // Menos
            System.Console.WriteLine(-valorNegativo); // inverte o sinal

            System.Console.WriteLine(!booleano); // negação lógica

            // incremento
            numero1++; // sintaxe utilizando operador posfixado
            System.Console.WriteLine(numero1);

            // decremento
            --numero1; // sintaxe utilizando operador prefixada
            System.Console.WriteLine(numero2);

            // numero2 será decrementado antes da comparação e numero1 depois
            System.Console.WriteLine(numero1++ == --numero2); // não favorece uma boa leitura do código

            System.Console.WriteLine($"{numero1} {numero2}");

            // melhor qualidade é deixar o codigo facil de ser entendido
            // cuidado para não ter código de dificil leitura

        }
    }
}