using System;
using System.Collections.Generic;
using System.Text;

namespace CursoCSharpCod3rUdemy.S03Fundamentos
{
    public class NotacaoPonto
    {
        public static void Executar() 
        {
            // estrategia de encadear varias chamadas em uma única senteça de código
            var saudacao = "Olá".ToUpper()
                .Insert(3, " World!")
                .Replace("World!", "Mundo!");

            Console.WriteLine(saudacao);

            Console.WriteLine("Teste".Length);

            // A notação ponto ela pode ser utilizada em todo o projeto, mas ela da problemas com valores nulos
            // Mas quanto a isso tem um simbolo (?) que ajuda a identificar e só seja acessado quando a variavel esta definida.
            string valorImportante = null;
            Console.WriteLine(valorImportante?.Length);
        }
    }
}