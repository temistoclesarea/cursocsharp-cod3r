using System;

namespace CursoCSharpCod3rUdemy.S03Fundamentos
{
    public class OperadoresRelacionais
    {
        public static void Executar()
        {
            // double nota = 6.0;
            System.Console.WriteLine("Digite a nota: ");
            double.TryParse(Console.ReadLine(), out double nota);
            double notaDeCorte = 7.0;

            System.Console.WriteLine("Nota inválida? {0}", nota > 10);
            System.Console.WriteLine("Nota inválida? {0}", nota < 0);
            System.Console.WriteLine("Perfeito? {0}", nota == 10.0);
            System.Console.WriteLine("Tem como melhorar? {0}", nota != 10.0);
            System.Console.WriteLine("Passou por média? {0}", nota >= notaDeCorte);
            System.Console.WriteLine("Recuperação? {0}", nota < notaDeCorte);
            System.Console.WriteLine("Reprovado? {0}", nota <= 3.0);
        }
    }
}