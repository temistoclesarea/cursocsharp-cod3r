using System;

namespace CursoCSharpCod3rUdemy.S03Fundamentos
{
    public class VariaveisEConstantes
    {
        public static void Executar()
        {
            // area da circunferencia
            // C# é fortemente tipada - é necessario definir o tipo
            // pensar em bons nomes para as variaveis e constantes
            // palavra reservada não podem ser usadas nos identificadores
            double raio = 4.5;
            const double PI = 3.1415;
            
            raio = 5.5;

            double area = PI * raio * raio;
            Console.WriteLine(area);
            Console.WriteLine("Área é " + area);

            // Tipos internos
            bool estaChovendo = true;
            Console.WriteLine("Está chovendo " + estaChovendo);

            // byte 0 até 256
            // short
            // int
            // long
            byte idade = 45; 
            Console.WriteLine("Idade " + idade);

            // sbyte tem sinal -127 até 128
            sbyte saldoDeGols = sbyte.MinValue; // pega o valor minimo aceito do tipo
            Console.WriteLine("Saldo de gols " + saldoDeGols);

            short salario = short.MaxValue;
            Console.WriteLine("Salario " + salario);

            int menorValorInt = int.MinValue; // int é o mais utilizando dos inteiros!
            Console.WriteLine("Menor int " + menorValorInt);

            uint populacaoBrasileira = 207_600_000; // pode ser utilizado o underscore para separar os milhares ou separar outros caracteristicas no numero e da clareza no numero
            Console.WriteLine("População Brasileira " + populacaoBrasileira);

            long menorValorLong = long.MinValue;
            Console.WriteLine("Menor long " + menorValorLong);

            ulong populacaoMundial = 7_600_000_000;
            Console.WriteLine("População Mundial " + populacaoMundial);

            // f para saber que é do tipo float
            // double é o dobro do float
            float precoComputador = 1299.99f;
            Console.WriteLine("Preço do computador " + precoComputador);

            double valorDeMercadoDaApple = 1_000_000_000_000.00; // mais utilizado
            Console.WriteLine("Valor Apple " + valorDeMercadoDaApple);

            decimal distanciaEntreEstrelas = decimal.MaxValue;
            Console.WriteLine("Distância entre estrelas " + distanciaEntreEstrelas);

            char letra = 'b';
            Console.WriteLine("Letra " + letra);

            string texto = "Seja bem vindo ao curso de C#";
            Console.WriteLine(texto);
            
        }
    }
}