using System;

namespace CursoCSharpCod3rUdemy.S03Fundamentos
{
    public class InferenciaTipo
    {
        public static void Executar()
        {
            // inferencia de tipo com var
            // é necessario já inicializar a variavel
            var nome = "Temistocles"; 
            // nome = 123; 
            Console.WriteLine(nome);

            var idade = 32; // inferencia var é necessario já inicializar a variavel
            Console.WriteLine(idade);

            // definindo o tipo, não é necessario inicializar a variavel
            int a;
            a = 3;
            int b = 2;
            Console.WriteLine(a + b);
            
        }
    }
}
