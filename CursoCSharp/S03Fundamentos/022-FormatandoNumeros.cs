using System;
using System.Globalization;

namespace CursoCSharpCod3rUdemy.S03Fundamentos
{
    public class FormatandoNumeros
    {
        public static void Executar()
        {
            double valor = 15.175;
            Console.WriteLine(valor.ToString("F1"));
            Console.WriteLine(valor.ToString("C"));
            Console.WriteLine(valor.ToString("P"));
            Console.WriteLine(valor.ToString("#.##"));

            CultureInfo cultura1 = new CultureInfo("pt-BR");
            Console.WriteLine(valor.ToString("C2"), cultura1);

            CultureInfo cultura2 = new CultureInfo("en-US");
            Console.WriteLine(valor.ToString("C3"), cultura2);

            int inteiro = 256;
            Console.WriteLine(inteiro.ToString("D10"));

        }
    }
}