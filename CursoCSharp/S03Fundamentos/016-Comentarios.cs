using System;

namespace CursoCSharpCod3rUdemy.S03Fundamentos
{
    /// <summary>
    /// 
    /// </sumary>
    public class Comentarios
    {
        public static void Executar()
        {
            // Cuidado com os comentários desnecessários
            Console.WriteLine("Código claro é sempre melhor!");

            /*
             * Esse é um comentário
             * de múltiplas linhas...
             */
            Console.WriteLine("O C# tem o XML Comments");
        }
        // Tenha um codigo limpo e só comente se realmente for necessario
    }
}
