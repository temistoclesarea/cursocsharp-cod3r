namespace CursoCSharpCod3rUdemy.S03Fundamentos
{
    public class OperadoresLogicos
    {
        public static void Executar()
        {
            /* 
            Tabela Verdade - Conhecimento básico de lógica

            AND
            V && V = V
            V && F = F
            F && V = F
            F && F = F

            OR
            V || V = V
            V || F = V
            F || V = V
            F || F = F

            XOR
            V ^ V = F
            V ^ F = V
            F ^ V = V
            F ^ F = F
            */

            var executouTrabalho1 = false;
            var executouTrabalho2 = false;

            // Operadores binarios = opera sobre dois operandos
            bool comprouTv50 = executouTrabalho1 && executouTrabalho2;
            System.Console.WriteLine("Comprou a Tv 50? {0}", comprouTv50);

            bool comprouSorvete = executouTrabalho1 || executouTrabalho2;
            System.Console.WriteLine("Comprou o sorvete? {0}", comprouSorvete);

            bool comprouTv32 = executouTrabalho1 ^ executouTrabalho2;
            System.Console.WriteLine("Comprou a Tv 32? {0}", comprouTv32);

            // Operador unario - opera sobre 1 operando
            System.Console.WriteLine("Mais saudável? {0}", !comprouSorvete);

            // Operador ternacio - opera sobre 3 operandos
            
        }
    }
}