using System;
using System.Collections.Generic;

namespace CursoCSharpCod3rUdemy.S09Excecoes
{
    public class NegativoException : Exception
    {
        public NegativoException() { }

        public NegativoException(string message) : base(message) { }

        // Uma exceção pode ter uma outra exceção como causa. Ex. queda de conexão
        public NegativoException(string message, Exception inner) 
            : base(message, inner) { }
    }

    public class ImparException : Exception
    {
        public ImparException(string message) : base(message) { }
    }

    public class ExcecoesPersonalizadas
    {
        public static int PositivoPar()
        {
            Random random = new Random();

            int valor = random.Next(-30, 31);

            // exemplo pra mostrar onde o método termina com sucesso e onde termina com erro
            if (valor < 0) {
                throw new NegativoException("Número negativo... :(");
            }

            if (valor % 2 == 1) {
                throw new ImparException("Valor impar... :(");
            }

            return valor;
        }
        public static void Executar() 
        {
            // pode fazer um laço pra tentar varias vezes até conseguir sucesso
            int tentativa = 1;
            while (tentativa <= 10) {
                Console.WriteLine("Tentativa " + tentativa);
                try {
                    Console.WriteLine(PositivoPar());
                } catch(NegativoException ex) {
                    Console.WriteLine(ex.Message);
                } catch(ImparException ex) {
                    Console.WriteLine(ex.Message);
                }
                tentativa++;
                Console.WriteLine();
            }

            // Exceções são importantes para não lançar mensagens de erro que não faça sentido para o usuario.
            // Dica: Colocar mensagens de erro tecnicas nos arquivos de log do sistema, e na mensagem tratada e diz que ocorreu um erro inesperado, o codigo do erro é XX e liga para o suporte, o suporte vai pegar o código e rastrear o erro nos logs de forma mais detalhada.
            // Cuidado ao mostrar erros para os usuários e até de segurança
            // Mensagens inteligiveis
        }
    }
}
