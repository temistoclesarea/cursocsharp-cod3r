using System;
using System.Collections.Generic;

namespace CursoCSharpCod3rUdemy.S09Excecoes
{
    public class Conta
    {
        double Saldo;

        public Conta(double saldo)
        {
            Saldo = saldo;
        }

        public void Sacar(double valor)
        {
            if (valor > Saldo) {
                // para execução do metodo e lançar a exceção
                // lançar uma mensagem com a causa do erro para o usuario
                // se tirar o throw, não vai lançar a exceção no try
                throw new ArgumentException("Saldo insuficiente.");
            }

            Saldo -= valor;
        }
    }
    public class PrimeiraExcecao
    {
        public static void Executar() 
        {
            var conta = new Conta(1_223.45);

            // Se você estiver usando um método que pode causar um erro, é importante usar o try
            // Parse tambem pode causar um erro, é um metodo arriscado, importante usar o try
            try {
                //int.Parse("abc");
                conta.Sacar(1600);
                Console.WriteLine("Retirada com sucesso!");
            // é possivel botar o tipo mais generico no parametro como tambem o tipo mais especifico
            // existe uma relação de herança entre os Exception, chamando todos os filhos
            } catch (Exception ex) {
                Console.WriteLine(ex.GetType().Name);
                Console.WriteLine(ex.Message);
                
            // finally sempre vai ser executado
            // garantir que uma ação seja feito tanto no caso de sucesso como de exceção
            } finally {
                Console.WriteLine("Obrigado!");
            }

            // Exceções não são obrigado tratar em C#, todas as exceções são não checadas, pois não cria dependencias.
        }
    }
}
