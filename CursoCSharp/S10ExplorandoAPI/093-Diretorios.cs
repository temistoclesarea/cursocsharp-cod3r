using System;
using System.IO;

namespace CursoCSharpCod3rUdemy.S10ExplorandoAPI
{    
    public class Diretorios
    {

        public static void Executar() 
        {
            var novoDir = @"~/PastaCSharp".ParseHome();
            var novoDirDestino = @"~/PastaCSharpDestino".ParseHome();
            var dirProjeto = @"C:/Projects CSharp/CursoCSharp-Cod3r-Udemy/CursoCSharp";

            // Se o diretorio existir, ele exclui o diretorio
            if (Directory.Exists(novoDir)) {
                // o parametro é para definir se vai excluir de forma recursiva ou não
                Directory.Delete(novoDir, true);
            }

            if (Directory.Exists(novoDirDestino)) {
                Directory.Delete(novoDirDestino, true);
            }

            Directory.CreateDirectory(novoDir);

            // mostra a data da criação do diretorio
            Console.WriteLine(Directory.GetCreationTime(novoDir));

            Console.WriteLine("====== Pastas ============");
            var pastas = Directory.GetDirectories(dirProjeto);

            foreach(var pasta in pastas) {
                Console.WriteLine(pasta);
            }

            Console.WriteLine("\n\n====== Arquivos ============");
            var arquivos = Directory.GetFiles(dirProjeto);

            foreach(var arquivo in arquivos) {
                Console.WriteLine(arquivo);
            }

            Console.WriteLine("\n\n====== Raiz ============");
            Console.WriteLine(Directory.GetDirectoryRoot(novoDir));

            // move um diretorio para outro
            Directory.Move(novoDir, novoDirDestino);

        }

    }
}
