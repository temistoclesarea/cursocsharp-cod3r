using System;
using System.IO;

namespace CursoCSharpCod3rUdemy.S10ExplorandoAPI
{    
    public class ExemploDateTime
    {
        public static void Executar() 
        {
            // pode usar paramentros nomeados
            //var dateTime = new DateTime(2030, 2, 6); 
            var dateTime = new DateTime(day: 6, month: 2, year: 2030);

            Console.WriteLine(dateTime.Day);
            Console.WriteLine(dateTime.Month);
            Console.WriteLine(dateTime.Year);

            // Sem horas
            var hoje = DateTime.Today;
            Console.WriteLine(hoje);

            // Com horas
            var diaAtual = DateTime.Now;
            Console.WriteLine(diaAtual);
            Console.WriteLine("Hora: " + diaAtual.Hour);
            Console.WriteLine("Minutos: " + diaAtual.Minute);

            var amanha = diaAtual.AddDays(1);
            Console.WriteLine(amanha);

            var ontem = diaAtual.AddDays(-1);
            Console.WriteLine(ontem);

            Console.WriteLine(diaAtual.ToString("dd")); // só o dia
            Console.WriteLine(diaAtual.ToString("d")); // dia atual
            Console.WriteLine(diaAtual.ToString("D")); // formato mais descritivo
            Console.WriteLine(diaAtual.ToString("g")); // dia atual com a hora sem segundos
            Console.WriteLine(diaAtual.ToString("G")); // dia atual com a hora e segundos
            Console.WriteLine(diaAtual.ToString("dd-MM-yyyy HH:mm")); // personalizado 
        }
    }
}
