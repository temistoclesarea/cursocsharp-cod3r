using System;
using System.IO;

namespace CursoCSharpCod3rUdemy.S10ExplorandoAPI
{    
    public class LendoArquivos
    {
        public static void Executar() 
        {
            var path = @"~/lendo_arquivos.txt".ParseHome();

            // using não pode ser usado com qualquer tipo de dados
            if (!File.Exists(path)) {
                using (StreamWriter sw = File.AppendText(path)) {
                    sw.WriteLine("Produto;Preco;Qtde");
                    sw.WriteLine("Caneta Bic Preta;3.59;89");
                    sw.WriteLine("Borracha;2.89;27");
                }
            }

            try {
                using (StreamReader sr = new StreamReader(path/*  + ".txt" */)) {
                    var texto = sr.ReadToEnd();
                    Console.WriteLine(texto);
                }
                // using garante que os recursos seram desalocados apos o uso
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
