using System;
using System.IO;

namespace CursoCSharpCod3rUdemy.S10ExplorandoAPI
{    
    public class ExemploFileInfo
    {

        // este metodo vai excluir todos os arquivos que forem passados por paramentros
        public static void ExcluirSeExistir(params string[] caminhos)
        {
            foreach (var caminho in caminhos) {
                FileInfo arquivo = new FileInfo(caminho);

                // deleta se existir
                if (arquivo.Exists) {
                    arquivo.Delete();
                }
            }
        }

        public static void Executar() 
        {
            var caminhoOrigem = @"~/arq_origem.txt".ParseHome();
            var caminhoDestino = @"~/arq_destino.txt".ParseHome();
            var caminhoCopia = @"~/arq_copia.txt".ParseHome();

            ExcluirSeExistir(caminhoOrigem, caminhoDestino, caminhoCopia);

            using (StreamWriter sw = File.CreateText(caminhoOrigem)) {
                sw.WriteLine("Arquivo original!");
            }

            // em cima de origem temos acesso a varias informações do arquivo
            FileInfo origem = new FileInfo(caminhoOrigem);

            Console.WriteLine(origem.Name);
            Console.WriteLine(origem.IsReadOnly);
            Console.WriteLine(origem.FullName);
            Console.WriteLine(origem.Extension);
            Console.WriteLine(origem.Directory);
            Console.WriteLine(origem.DirectoryName);

            origem.CopyTo(caminhoCopia); // gerar uma copia
            origem.MoveTo(caminhoDestino); // move o arquivo
        }

    }
}
