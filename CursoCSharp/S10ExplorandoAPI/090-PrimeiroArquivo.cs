using System;
using System.IO;

namespace CursoCSharpCod3rUdemy.S10ExplorandoAPI
{
    public static class ExtensaoString
    {
        // função de extenção
        public static string ParseHome(this string path) {
            string home = (Environment.OSVersion.Platform == PlatformID.Unix ||
                Environment.OSVersion.Platform == PlatformID.MacOSX)
                ? Environment.GetEnvironmentVariable("HOME") :
                Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%");
            return path.Replace("~", home);
        }
    }
    
    public class PrimeiroArquivo
    {
        public static void Executar() 
        {
            // \t é um tab, \n é quebra de linha
            // colocando @ ele impede a interpretação desses caracteres
            // var s = "\teste\nestes\a.txt";
            // Console.WriteLine(s);

            // colocando @ ele interpreta a string de forma literal, sem os códigos
            // se usa o ~ para interpretar a pasta home do usuario
            // poderia usar o caminho absoluto
            // windows e linux tem comportamentos diferentes
            var path = @"~/primeiro_arquivo.txt".ParseHome();

            // se o arquivo não existir
            if (!File.Exists(path)) {
                // dentro do contexto da escrita de arquivo, o C# aloca alguns recursos do sistema operacional que é importante que eles sejam fechados quando sair do arquivo
                using (StreamWriter sw = File.CreateText(path)) {
                    sw.WriteLine("Esse é");
                    sw.WriteLine("o nosso");
                    sw.WriteLine("primeiro");
                    sw.WriteLine("arquivo!");
                }
                // ao sair do bloco, o C# vai fechar todos os recursos utilizados para escrita de arquivo acima
            }

            // altera um arquivo já existente
            // não acessa o primeiro, pois o arquivo já esta criado e já existe
            using (StreamWriter sw = File.AppendText(path)) {
                sw.WriteLine("");
                sw.WriteLine("É possível");
                sw.WriteLine("adicionar");
                sw.WriteLine("mais texto!");
            }
        }
    }
}
