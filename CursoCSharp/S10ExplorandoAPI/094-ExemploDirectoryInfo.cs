using System;
using System.IO;

namespace CursoCSharpCod3rUdemy.S10ExplorandoAPI
{    
    public class ExemploDirectoryInfo
    {
        public static void Executar() 
        {
            var dirProjeto = @"C:/Projects CSharp/CursoCSharp-Cod3r-Udemy/CursoCSharp";

            // inferencia - lembrar
            var dirInfo = new DirectoryInfo(dirProjeto);

            if (!dirInfo.Exists) {
                dirInfo.Create();
            }

            Console.WriteLine("== Arquivos ===============");
            var arquivos = dirInfo.GetFiles();

            foreach (var arquivo in arquivos) {
                Console.WriteLine(arquivo);
            }

            Console.WriteLine("\n\n== Diretorios ===============");
            var pastas = dirInfo.GetDirectories();

            foreach (var pasta in pastas) {
                Console.WriteLine(pasta);
            }

            Console.WriteLine(dirInfo.CreationTime);
            Console.WriteLine(dirInfo.FullName);
            Console.WriteLine(dirInfo.Root);
            Console.WriteLine(dirInfo.Parent.Parent); // parent retorna tambem um DirInfo
        }
    }
}
