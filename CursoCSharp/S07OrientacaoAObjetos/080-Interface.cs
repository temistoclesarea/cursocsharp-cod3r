using System;
using System.Collections.Generic;

namespace CursoCSharpCod3rUdemy.S07OrientacaoAObjetos
{
    // Classe concreta é 100% implementada
    // Classe abstrata é 100% implementada ou 100% não implementada
    // Interface é uma classe abstrata onde todos os métodos são abstratos
    // Interface pode ter uma classe implementando multiplas interfaces
    // Pode ter herança multiplas de interfaces, mas não de classes
    // implicitamente interface já é 100% abstrato, não necessitando de abstract
    // por padrão todos os métodos são publicos
    
    interface Teste
    {
        bool Bla(string a);
    }

    interface OperacaoBinaria
    {
        int Operacao(int a, int b); // só define a estrutura
    }

    // uma classe que implementa duas interfaces
    // vocé é obrigado a implementar todos os metodos definidos na interface
    class Soma : OperacaoBinaria, Teste
    {
        // obrigatoriamente, todos os membros de interface são publicos
        // não pode diminuir a visibilidade
        public int Operacao(int a, int b)
        {
            return a + b;
        }

        public bool Bla(string teste)
        {
            return true;
        }
    }

    class Subtracao : OperacaoBinaria
    {
        public int Operacao(int a, int b)
        {
            return a - b;
        }
    }

    class Multiplicacao : OperacaoBinaria
    {
        public int Operacao(int a, int b)
        {
            return a * b;
        }
    }

    class Calculadora
    {
        List<OperacaoBinaria> operacoes = new List<OperacaoBinaria> {
            new Soma(),
            new Subtracao(),
            new Multiplicacao(),
        };

        public string ExecutarOperacoes(int a, int b)
        {
            string resultado = "";

            foreach(var op in operacoes) {
                // op.GetType().Name pega o nome da classe
                resultado += $"Usando {op.GetType().Name} = {op.Operacao(a, b)}\n";
            }

            return resultado;
        }
    }

    
    public class Interface
    {
        public static void Executar() 
        {
            var calc = new Calculadora();
            var resultado = calc.ExecutarOperacoes(20, 5);

            Console.WriteLine(resultado);
        }
    }
}
