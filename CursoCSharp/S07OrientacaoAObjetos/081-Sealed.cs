using System;
using System.Collections.Generic;

namespace CursoCSharpCod3rUdemy.S07OrientacaoAObjetos
{
    // Classe Selada
    // Contrario da classe abstrata
    // Classe selada não permite herança
    // Não consegue sobrescrever os membros

    sealed class SemFilho
    {
        public double ValorDaFortuna()
        {
            return 1_407_033.65;
        }
    }

    // não é possivel herdar
    // class SouFilho : SemFilho
    // {}

    class Avo
    {
        // virtual permite ser sobrescrito
        public virtual bool HonrarNomeFamilia()
        {
            return true;
        }
    }

    class Pai : Avo
    {
        public override sealed bool HonrarNomeFamilia()
        {
            return true;
        }
    }

    class FilhoRebelde : Pai
    {
        // não é possivel sobrecrever, mas é possivel ocultar com new
        /* public override bool HonrarNomeFamilia()
        {
            return false;
        } */
    }

    public class Sealed
    {
        public static void Executar() 
        {
            SemFilho semFilho = new SemFilho();
            Console.WriteLine(semFilho.ValorDaFortuna());

            FilhoRebelde filho = new FilhoRebelde();
            Console.WriteLine(filho.HonrarNomeFamilia());
        }
    }
}
