using System;

namespace CursoCSharpCod3rUdemy.S07OrientacaoAObjetos
{
    // Classe base Comida
    public class Comida
    {
        public double Peso;

        public Comida(double peso)
        {
            Peso = peso;
        }

        public Comida() {}
    }

    public class Feijao : Comida
    {
        //public double Peso;
        public Feijao(double peso) : base(peso) { }
    }

    public class Arroz : Comida
    {
        //public double Peso;
    }

    public class Carne : Comida
    {
        //public double Peso;
    }

    public class Salada : Comida
    {

    }

    // Pessoa : Comida -> não deve promover reuso forçando a herança
    // Não forçe uma herança se realmente não há herança entre as duas classes, pode gerar código ruim e problemas futuros
    public class Pessoa
    {
        public double Peso;

        // Não é mais necessario 3 métodos para cada comida
        /* public void Comer (Feijao feijao)
        {
            Peso += feijao.Peso;
        }

        public void Comer (Arroz arroz)
        {
            Peso += arroz.Peso;
        }

        public void Comer (Carne carne)
        {
            Peso += carne.Peso;
        } */

        // em comida pode passar tipos mais especificos, que herdam de comida
        public void Comer(Comida comida)
        {
            Peso += comida.Peso;
        }
    }

    public class Polimorfismo
    {
        public static void Executar() 
        {
            Feijao ingrediente1 = new Feijao(0.3);
            //ingrediente1.Peso = 0.30;

            Arroz ingrediente2 = new Arroz();
            ingrediente2.Peso = 0.25;

            Carne ingrediente3 = new Carne();
            ingrediente3.Peso = 0.3;

            Salada ingrediente4 = new Salada();
            ingrediente4.Peso = 0.1;

            Pessoa cliente = new Pessoa();
            cliente.Peso = 80.2;

            cliente.Comer(ingrediente1);
            cliente.Comer(ingrediente2);
            cliente.Comer(ingrediente3);
            cliente.Comer(ingrediente4);

            Console.WriteLine($"Agora o peso do cliente é {cliente.Peso.ToString("F")} Kg!");

            // Capacidade de atribuir tipos mais especificos, para variaveis de tipos mais genericos

        }
    }
}
