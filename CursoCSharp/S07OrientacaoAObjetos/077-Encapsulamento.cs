using System;
using Encapsulamento; // precisa importar o outro projeto com o nome do namespace

namespace CursoCSharpCod3rUdemy.S07OrientacaoAObjetos
{

    // Herda de SubCelebridade mas não esta no mesmo projeto
    public class FilhoNaoReconhecido : SubCelebridade
    {
        // ocultando com new
        public new void MeusAcessos()
        {
            Console.WriteLine("FilhoNaoReconhecido...");

            Console.WriteLine(InfoPublica); // todos acessam
            Console.WriteLine(CorDoOlho); // acessa por herança
            //Console.WriteLine(NumeroCelular); // só acesso no mesmo projeto
            Console.WriteLine(JeitoDeFalar); // acessa por herança ou dentro do mesmo projeto
            //Console.WriteLine(SegredoFamilia); // acessa por herança apenas dentro do projeto
            //Console.WriteLine(UsaMuitoPhotoshop); // ninguem acessa
            Console.WriteLine();
        }
    }

    // Não herda e não esta no mesmo projeto
    public class AmigoDistante
    {
        // Como não tem relação de Herança, tem que ter uma Relação de composição
        // Dentro da Classe tem que ter uma instancia(referencia) de SubCelebridade
        public readonly SubCelebridade amiga = new SubCelebridade();

        // ocultando com new
        public void MeusAcessos()
        {
            Console.WriteLine("AmigoDistante...");

            Console.WriteLine(amiga.InfoPublica); // só esse vai estar acessivel
            // Console.WriteLine(amiga.CorDoOlho); // não recebe por herança
            // Console.WriteLine(amiga.NumeroCelular); // só no projeto
            // Console.WriteLine(amiga.JeitoDeFalar); // ou no projeto ou por herança
            // Console.WriteLine(amiga.SegredoFamilia); // só por herança
            // Console.WriteLine(amiga.UsaMuitoPhotoshop); // privado
            Console.WriteLine();
        }
    }


    public class Encapsulamento
    {
        public static void Executar() 
        {
            SubCelebridade sub = new SubCelebridade();
            sub.MeusAcessos();

            // é possivel instanciar direto e chamar na mesma linha, sem atribuir uma variavel
            // por não usar mais nada, faz dessa forma
            new FilhoReconhecido().MeusAcessos();
            new AmigoProximo().MeusAcessos();
            new FilhoNaoReconhecido().MeusAcessos();
            new AmigoDistante().MeusAcessos();

            // Ao escolher o nivel de visibilidade dos atributos e metodos, começe pelo nivel de visibilidade mais restrito e a medida que for surgindo cenarios validos para aumentar o nivel de visibilidade de algum metodo, tente aumentar gradativamente para atender as demandas ao inves de colocar publico em todos, pois vai gerar problemas no futuro.
            
        }
    }
}
