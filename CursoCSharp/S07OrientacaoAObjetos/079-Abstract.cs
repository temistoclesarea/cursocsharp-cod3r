using System;
using System.Collections.Generic;

namespace CursoCSharpCod3rUdemy.S07OrientacaoAObjetos
{
    //Abstract da vida a classe
    //não pode instanciar classe abstrata, só classes concretas
    //uma classe abstrata pode ter todos os metodos implementados
    //pode ter todos os metodos abstratos
    //a classe abstrada é inacabada, quem herda que vai implementar seus metodos
    public abstract class Celular
    {
        public abstract string Assistente();

        public string Tocar()
        {
            return "Trim trim trim...";
        }
    }

    public class Samsung : Celular
    {
        // tem que colocar override, mesmo sendo abstrato
        // obrigatorio implementar Assistente()
        // Tocar() já herda por herança
        public override string Assistente()
        {
            return "Olá! Meu nome é Bixby!";
        }
    }

    public class IPhone : Celular
    {
        public override string Assistente()
        {
            return "Olá! Meu nome é Siri!";
        }
    }

    public class Abstract
    {
        public static void Executar() 
        {
            //Celular c = new Celular(); // não é possivel instanciar classes abstratas
            var celulares = new List<Celular> {
                new IPhone(),
                new Samsung(),
            };

            foreach (var celular in celulares) {
                Console.WriteLine(celular.Assistente());
            }
        }
    }
}
