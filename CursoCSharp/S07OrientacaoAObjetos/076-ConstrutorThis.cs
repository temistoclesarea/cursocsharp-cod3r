using System;

namespace CursoCSharpCod3rUdemy.S07OrientacaoAObjetos
{
    public class Animal{
        public string Nome { get; set; }

        public Animal(string nome)
        {
            Nome = nome;
        }
    }

    public class Cachorro : Animal 
    {
        public double Altura { get; set; }

        // obrigatoriamente tem que passar o base(nome) pois Animal não tem o construtor padrão sendo assim obrigatorio definir aqui
        public Cachorro(string nome) : base(nome)
        {
            Console.WriteLine($"Cachorro {nome} inicializado");
        }

        // :this(nome) chama o construtor dessa propria classe, que chama o base e volta para este
        public Cachorro(string nome, double altura) : this(nome)
        {
            Altura = altura;
        }

        public override string ToString()
        {
            return $"{Nome} tem {Altura}cm de altura!";
        }
    }
    public class ConstrutorThis
    {
        public static void Executar() 
        {
            var spike = new Cachorro("Spike");
            var max = new Cachorro("Max", 40.0);

            Console.WriteLine(spike); // chamando implicitamente o toString()
            Console.WriteLine(max.ToString()); // chamando explicitamente toString()
        }
    }
}
