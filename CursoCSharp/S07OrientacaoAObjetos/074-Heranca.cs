using System;

namespace CursoCSharpCod3rUdemy.S07OrientacaoAObjetos
{
    public class Carro {
        protected readonly int VelocidadeMaxima; // protected será transmitido por herança
        int VelocidadeAtual;

        //public Carro() {}

        public Carro(int velocidadeMaxima) 
        {
            VelocidadeMaxima = velocidadeMaxima;
        }

        // Velocidade não pode ser mexida por muita gente, só transmitido por herança
        // Definir que a velocidade não pode ser negativa e nem passar velocidade maxima
        protected int AlterarVelocidade(int delta)
        {
            int novaVelocidade = VelocidadeAtual + delta;

            if (novaVelocidade < 0) {
                VelocidadeAtual = 0;
            } else if (novaVelocidade > VelocidadeMaxima) {
                VelocidadeAtual = VelocidadeMaxima;
            } else {
                VelocidadeAtual = novaVelocidade;
            }

            return VelocidadeAtual;
        }

        // Cada carro vai ter um metodo de acelerar diferente
        // por padrão acelerar vai ser 5 km/h
        // virtual diz que o metodo pode ser sobrescrito pelas classes filhas
        // sem o virtual o metodo não pode ser sobrescrito
        public virtual int Acelerar()
        {
            return AlterarVelocidade(5);
        }

        public int Frear()
        {
            return AlterarVelocidade(-5);
        }
    }

    
    public class Uno : Carro
    {
        // construtor automatico de forma implicita ou explicita
        // por não ter construtor padrão tem 
        public Uno() : base(200) {}
    }

    public class Ferrari : Carro
    {
        public Ferrari() : base(350) {}

        // override esta sobrescrevendo a classe pai
        // ao sobrescrever, tentar manter a compatibilidade dos metodos
        // existe um principio solid para manter esse padrão
        public override int Acelerar()
        {
            return AlterarVelocidade(15);
        }

        // este vai oculta o metodo da classe pai com operador new
        public new int Frear()
        {
            return AlterarVelocidade(-15);
        }
    }

    public class Heranca
    {
        public static void Executar() 
        {
            // mantem a dependencia mais saudavel encapsulando os metodos

            Console.WriteLine("Uno...");
            Uno carro1 = new Uno();
            Console.WriteLine(carro1.Acelerar());
            Console.WriteLine(carro1.Acelerar());
            Console.WriteLine(carro1.Frear());
            Console.WriteLine(carro1.Frear());

            Console.WriteLine("Ferrari...");
            Ferrari carro2 = new Ferrari();
            Console.WriteLine(carro2.Acelerar());
            Console.WriteLine(carro2.Acelerar());
            Console.WriteLine(carro2.Frear());
            Console.WriteLine(carro2.Frear());
            Console.WriteLine(carro2.Frear());

            Console.WriteLine("Ferrari com tipo Carro...");
            Carro carro3 = new Ferrari(); // Polimorfismo
            // acelerar vai acessar o metodo sobrescrito da classe filha, pois usou virtual
            Console.WriteLine(carro3.Acelerar()); 
            Console.WriteLine(carro3.Acelerar()); 
            // frear vai usar o metodo da classe pai, pois usou new
            Console.WriteLine(carro3.Frear()); 
            Console.WriteLine(carro3.Frear());
            Console.WriteLine(carro3.Frear());

            Console.WriteLine("Uno com tipo Carro...");
            carro3 = new Uno(); // Polimorfismo
            Console.WriteLine(carro3.Acelerar());
            Console.WriteLine(carro3.Acelerar());
            Console.WriteLine(carro3.Frear());
            Console.WriteLine(carro3.Frear());
            Console.WriteLine(carro3.Frear());

            // ocultar o metodo com new pode gerar comportamentos diferentes, apesar da flexibilidade da ocultação

        }
    }
}
