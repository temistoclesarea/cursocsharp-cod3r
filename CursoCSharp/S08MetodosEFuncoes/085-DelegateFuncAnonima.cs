using System;
using System.Collections.Generic;

namespace CursoCSharpCod3rUdemy.S08MetodosEFuncoes
{
    public class DelegateFuncAnonima
    {
        delegate string StringOperacao(string s);
        public static void Executar() 
        {
            // delegate associado com uma função anonima
            // nesta sintax, não usa a seta e precisa informar o tipo no parametro
            StringOperacao inverter = delegate (string s) {
                char[] charArray = s.ToCharArray();
                Array.Reverse(charArray);
                return new string(charArray);
            };

            Console.WriteLine(inverter("C# é show!!!"));
        }
    }
}
