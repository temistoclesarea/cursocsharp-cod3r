using System;
using System.Collections.Generic;

namespace CursoCSharpCod3rUdemy.S08MetodosEFuncoes
{
    public static class ExtencoesInteiro
    {
        public static int Soma(this int num, int outroNumero) {
            return num + outroNumero;
        }

        public static double Subtracao(this double num, double outroNumero) {
            return num - outroNumero;
        }
    }
    public class MetodosDeExtecao
    {
        public static void Executar() 
        {
            int numero = 5;
            double numeroReal = 5.5;

            Console.WriteLine(numero.Soma(3));
            Console.WriteLine(numeroReal.Subtracao(10));

            // possivel usar em cima de valores literais
            Console.WriteLine(2.Soma(3));
            Console.WriteLine(2.9.Subtracao(4.9).ToString("F2"));

            // capacidade de extender os tipos da linguagem
            //extender os tipos já criados
            //abre possibilidades muitos legais para funções que se usa muito frequentemente no código podendo estar presente onde faz mais sentido.
        }
    }
}
