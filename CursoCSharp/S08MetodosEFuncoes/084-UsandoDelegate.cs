using System;
using System.Collections.Generic;

namespace CursoCSharpCod3rUdemy.S08MetodosEFuncoes
{
    public class UsandoDelegates
    {
        delegate double Soma(double a, double b);
        delegate void ImprimirSoma(double a, double b);

        // não precisa ser o mesmo nome
        // MinhaSoma respeita as assinaturas do delegate Soma
        static double MinhaSoma(double x, double y)
        {
            return x + y;
        }

        static void MeuImprimirSoma(double a, double b)
        {
            Console.WriteLine(a + b);
        }

        public static void Executar() 
        {
            // é possivel pegar funções pré existentes e atribuir aos delegates, se compativeis
            Soma op1 = MinhaSoma;
            Console.WriteLine(op1(2, 3));

            ImprimirSoma op2 = MeuImprimirSoma;
            op2(5.4, 8);

            // da para usar os delegates com Func e Action tambem
            Func<double, double, double> op3 = MinhaSoma;
            Console.WriteLine(op3(2.5, 3));

            Action<double, double> op4 = MeuImprimirSoma;
            op4(7.7, 23.4);
        }
    }
}
