using System;
using System.Collections.Generic;

namespace CursoCSharpCod3rUdemy.S08MetodosEFuncoes
{
    // Estes capitulos seram focados mais no mundo da programação funcional
    // Possibilidades funcionais em C#
    // Torna a linguagem mais rica

    public class ExemploLambda
    {
        public static void Executar() 
        {
            // Lambda é uma função anonima que armazena ela em uma variavel
            // Action é uma função que não tem retorno, sempre retorno void
            // Action pode receber parametro tambem
            Action algoNoConsole = () => {
                Console.WriteLine("Lambda com C#!");
            };
            
            algoNoConsole();

            Action<string> algoNoConsole2 = (a) => {
                Console.WriteLine("Lambda com C#!" + a);
            };

            algoNoConsole2("!!!");

            // Função anonima com retorno
            Func<int> jogarDado = () => {
                Random random = new Random();
                return random.Next(1, 7); // 7 não entra nos valores aleatorios
            };

            Console.WriteLine(jogarDado());

            // quando tem 1 unico paramentro, não precisa colocar os parenteses
            // quando coloca o arrow, implicitamente tem um return
            // primeiro vem os parametros que seram passados
            // segundo vem o tipo de retorno que a função irá retornar
            // se colocar os parenteses, será necessario colocar o ; e o return
            // o ultimo paramentro sempre vai ser o retorno
            // se tiver só um paramentro, este sempre vai ser o retorno, sem paramentros
            Func<int, string> conversorHex1 = numero => numero.ToString("X");

            Func<int, string> conversorHex2 = (numero) => {
                return numero.ToString("X");
            };

            // resultado é o correspondente ao valor hexadecimal
            Console.WriteLine(conversorHex1(1234));
            Console.WriteLine(conversorHex2(4321));

            Func<int, int, int, string> formatarData = (dia, mes, ano) =>
                String.Format("{0:D2}/{1:D2}/{2:D4}", dia, mes, ano);

            Console.WriteLine(formatarData(1,1,2021));
        }
    }
}
