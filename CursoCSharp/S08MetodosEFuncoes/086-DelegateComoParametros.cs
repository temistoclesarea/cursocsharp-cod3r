using System;
using System.Collections.Generic;

namespace CursoCSharpCod3rUdemy.S08MetodosEFuncoes
{
    public class DelegateComoParametros
    {
        public delegate int Operacao(int x, int y);

        public static int Soma(int x, int y) {
            return x + y;
        }

        // passar uma funçao para outra função como paramentro
        public static string Calculadora(Operacao op, int x, int y) {
            var resultado = op(x, y);
            return "Resultado: " + resultado;
        }
        public static void Executar() 
        {
            Operacao subtracao = (int x, int y) => x - y;
            Console.WriteLine(Calculadora(subtracao, 3, 2));

            Console.WriteLine(Calculadora(Soma, 3, 2));
        }
    }
}
