using System;
using System.Collections.Generic;

namespace CursoCSharpCod3rUdemy.S08MetodosEFuncoes
{
    // Delegate é definir um tipo que armazena a assinatura de uma função
    // Operação é um tipo
    // espera-se que seja atribuido uma funçao que seja atribuido uma assinatura
    delegate double Operacao(double x, double y);
    public class LambdaDelegate
    {
        public static void Executar() 
        {
            Operacao sum = (x, y) => x + y;    
            Operacao sub = (x, y) => x - y;
            Operacao mult = (x, y) => x * y;

            Console.WriteLine(sum(3, 3));   
            Console.WriteLine(sub(5, 2));   
            Console.WriteLine(mult(2, 8));   
        }
    }
}
