using System;
using System.Collections.Generic; // Generics

namespace CursoCSharpCod3rUdemy.S06Colecoes
{
    public class ColecoesSet
    {
        // Já esta usando a classe produto definido em List, por isso importante organizar por arquivo
        public static void Executar() 
        {
            var livro = new Produto("Game of thrones", 49.9);

            // HashSet não é indexado, não tem como pegar indexs
            // Set tambem não aceita repetição
            // Set é um conjunto e junta conjuntos
            var carrinho = new HashSet<Produto>(); 

            carrinho.Add(livro);

            var combo = new HashSet<Produto> {
                new Produto("Camisa", 29.9),
                new Produto("8ª Temporada Game of Thrones", 99.9),
                new Produto("Poster", 10),
                new Produto("Poster", 10), 
                // mesmo com o equals, ainda ficou duplicado, tipos diferentes para hashcode
                // implementando o hashcode, ele removeu o item duplicado
                livro, // não adiciona este, pois já existe
            };

            //carrinho.AddRange(combo);
            // é possivel fazer a união de um conjunto com uma lista
            carrinho.UnionWith(combo); //ele altera carrinho para união de outro conjunto

            Console.WriteLine(carrinho.Count);

            //carrinho.RemoveAt(3);

            foreach(var item in carrinho) {
                //Console.Write(carrinho.IndexOf(item));
                Console.WriteLine($" {item.Nome} {item.Preco}");
            }

            Console.WriteLine(carrinho.Count);

            carrinho.Add(livro); // não adiciona pois já existe

            Console.WriteLine(carrinho.Count);

            //Console.WriteLine(carrinho.LastIndexOf(livro));

            foreach(var item in carrinho) {
                //Console.Write(carrinho.IndexOf(item));
                Console.WriteLine($" {item.Nome} {item.Preco}");
            }
        }
    }
}
