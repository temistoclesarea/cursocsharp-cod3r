using System;
using System.Collections;
using System.Collections.Generic;

namespace CursoCSharpCod3rUdemy.S06Colecoes
{
    public class ColecoesQueue
    {
        public static void Executar() 
        {
            // Fila aceita repetição de itens
            var fila = new Queue<string>(); // Queue que usa generics, usa generic no import

            fila.Enqueue("Fulano"); // Enqueue adiciona itens na fila
            fila.Enqueue("Sicrano");
            fila.Enqueue("Beltrano");
            fila.Enqueue("Beltrano");

            Console.WriteLine(fila.Peek()); // Peek pega o primeiro elemento da fila, mas não remove

            // Nos arrays utiliza .Lenght nas coleções utiliza .Count, ambos atributos
            Console.WriteLine(fila.Count);

            Console.WriteLine(fila.Dequeue()); // Dequeue mostra e retira o elemento da fila
            Console.WriteLine(fila.Count);

            foreach(var pessoa in fila) {
                Console.WriteLine(pessoa);
            }

            // valores heterogeneos
            var salada = new Queue(); // Queue que usa collections, sem o generics

            salada.Enqueue(3);
            salada.Enqueue("Item");
            salada.Enqueue(true);
            salada.Enqueue(3.14);

            // procura um item na fila, diferencia maiusculas
            Console.WriteLine(salada.Contains("item"));
            Console.WriteLine(salada.Contains("Item"));
        }
    }
}
