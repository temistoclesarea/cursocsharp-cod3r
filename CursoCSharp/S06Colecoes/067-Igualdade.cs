using System;
using System.Collections;
using System.Collections.Generic;

namespace CursoCSharpCod3rUdemy.S06Colecoes
{
    public class Igualdade
    {
        public static void Executar() 
        {
            // Produto da classe Produto em list
            var p1 = new Produto("Caneta", 1.89); 
            var p2 = new Produto("Caneta", 1.89);
            var p3 = p2; // atribuição por referencia, são do mesmo local de memoria

            Console.WriteLine(p1 == p2); // comparando endereço de memoria
            // Ele é falso porque ele esta comparando referencias de memoria e instancias diferentes

            Console.WriteLine(p3 == p2); // comparando endereço de memoria
            // Ele é verdadeiro porque as duas variaveis apontam para o mesmo local de memoria, por referencia

            // Equals define se o metodo é igual ao outro produto
            // Equals faz a mesma coisa, compara endereço de memoria, porem pode ser alterado
            Console.WriteLine(p1.Equals(p2));
            // Teve de alterar o Equals na class Produto
            // Implementando Equals, temos a possibilidade de comparar um produto com outro em termos de valores e não endereço de memoria, pode ser tambem usado ID


            // é interessante ter o Equals e o HashCode dentros dos seus objetos para quando ele for colodado nas listas ou hashset, não seja comparado o endereço de memoria

            // comparar por endereço de memoria costuma não fazer sentido para a aplicação
            // Visual Studio Code não auto gera o Equals e HashCode, pesquisar...
        }
    }
}
