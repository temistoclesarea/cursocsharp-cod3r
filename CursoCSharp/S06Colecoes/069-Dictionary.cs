using System;
using System.Collections.Generic; // Dictionary tem que definir generic

namespace CursoCSharpCod3rUdemy.S06Colecoes
{
    public class ColecoesDictionary
    {
        public static void Executar() 
        {
            // Dictionary, estrutura chave e valor
            // dictionary não aceita repetição no int, mas aceita no string
            // chave não aceita repetição e valor aceita repetição
            var filmes = new Dictionary<int, string>();

            filmes.Add(2000, "Gladiador");
            filmes.Add(2002, "Homem-Aranha");
            filmes.Add(2004, "Os Incríveis");
            filmes.Add(2006, "O Grande Truque");

            // ContainsKey pega o valor e verifica se esta nas chaves
            if (filmes.ContainsKey(2004)) {
                Console.WriteLine("2004: " + filmes[2004]); 
                // filmes[2004] pega o valor contido
                // gera um erro se não existir a chave cadastrada, gera uma exceção

                Console.WriteLine("2004: " + filmes.GetValueOrDefault(2004)); 
                // retorna o valor ou uma string vazia
                // nesse caso, se não existir a chave, gera uma string vazia
            }

            Console.WriteLine(filmes.ContainsValue("Amnésia"));
            // Verifica se o valor esta contido no dicionario, compara pelo valor
            // retorna verdadeiro ou falso

            Console.WriteLine($"Removeu? {filmes.Remove(2004)}");
            // remove tambem verifica se a chave foi removida retornando verdadeiro ou falso

            filmes.TryGetValue(2006, out string filme2006); 
            // trygetvalue pega o valor passado por referencia
            // atribui o valor de 2006 a variavel filme2006
            // se não exitir vai um valor vazio
            Console.WriteLine($"Filme {filme2006}");

            //Percorer o Dicionario - 4 formas
            foreach (var chave in filmes.Keys) {
                Console.WriteLine(chave);
            }

            foreach (var valor in filmes.Values) {
                Console.WriteLine(valor);
            }

            foreach (KeyValuePair<int, string> filme in filmes) { // forma explicita, definindo
                Console.WriteLine($"{filme.Value} é de {filme.Key}.");
            }

            foreach (var filme in filmes) { // o compilador atribui por inferencia, forma implicita
                Console.WriteLine($"- {filme.Value} é de {filme.Key}.");
            }
        }
    }
}
