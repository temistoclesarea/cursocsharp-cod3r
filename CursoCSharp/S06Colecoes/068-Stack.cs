using System;
using System.Collections; // Stack necessita do collections, sem o generics

namespace CursoCSharpCod3rUdemy.S06Colecoes
{
    public class ColecoesStack
    {
        public static void Executar() 
        {
            // Stack é o contrario da fila, o primeiro a entrar é o ultimo a sair
            // Conceito de Pilha
            var pilha = new Stack();

            // Push adiciona itens na pilha
            pilha.Push(3);
            pilha.Push("a");
            pilha.Push(true);
            pilha.Push(3.14f);

            foreach (var item in pilha) {
                Console.Write($"{item} | ");
            }

            Console.WriteLine($"\nPop: {pilha.Pop()}");
            // pop remove o ultimo elemento adicionado

            foreach (var item in pilha) {
                Console.Write($"{item} | ");
            }

            Console.WriteLine($"\nPeek: {pilha.Peek()}");
            // peek pega o valor proximo da pilha, mas não remove
            // peek tambem funciona da mesma forma na fila
            Console.WriteLine(pilha.Count);


        }
    }
}
