using System;
using System.Collections; // tem que remover o generic em arraylist

namespace CursoCSharpCod3rUdemy.S06Colecoes
{
    public class ColecoesArrayList
    {
        public static void Executar() 
        {
            // não é muito recomendado pelas boas praticas armazenar varios tipos diferentes no array list, mas pode ser preciso em certos momentos.
            var arrayList = new ArrayList {
                "Palavra",
                3,
                true,
            };

            arrayList.Add(3.14);
            //arrayList.Count;

            foreach (var item in arrayList) {
                Console.WriteLine("{0} => {1}", item, item.GetType());
            }
        }
    }
}
