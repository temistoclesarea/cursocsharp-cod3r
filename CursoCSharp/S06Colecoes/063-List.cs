using System;
using System.Collections.Generic; // Generics

namespace CursoCSharpCod3rUdemy.S06Colecoes
{
    public class Produto
    {
        public string Nome;
        public double Preco;

        public Produto(string nome, double preco)
        {
            Nome = nome;
            Preco = preco;
        }

        // Altera Equals da aula de Igualdade
        // Equals é bem mais lento que o Hashcode
        public override bool Equals(object obj)
        {
            Produto outroProduto = (Produto)obj;
            bool mesmoNome = Nome == outroProduto.Nome;
            bool mesmoPreco = Preco == outroProduto.Preco;
            return mesmoNome && mesmoPreco;
        }

        // Indice de banco de dados
        // Hashcode é bem mais rapido
        public override int GetHashCode()
        {
            return Nome.Length;
        }

        // não é necessario implementar Equals e GetHashCode na mão, a propria ide pode implementar
    }
    public class ColecoesList
    {
        public static void Executar() 
        {
            var livro = new Produto("Game of thrones", 49.9);

            // Generics do ponto de vista do desenvolvedor
            // Especifica do ponto de vista de quem usa
            // interessante lista homeneas com os mesmo tipo de dados
            // list é uma estrutura de tamanho dinamico
            var carrinho = new List<Produto>(); // só armazena o tipo Produto

            carrinho.Add(livro);

            var combo = new List<Produto> {
                new Produto("Camisa", 29.9),
                new Produto("8ª Temporada Game of Thrones", 99.9),
                new Produto("Poster", 10),
            };

            carrinho.AddRange(combo);

            Console.WriteLine(carrinho.Count);

            carrinho.RemoveAt(3);

            foreach(var item in carrinho) {
                Console.Write(carrinho.IndexOf(item));
                Console.WriteLine($" {item.Nome} {item.Preco}");
            }


            Console.WriteLine(carrinho.Count);

            carrinho.Add(livro);

            Console.WriteLine(carrinho.Count);

            Console.WriteLine(carrinho.LastIndexOf(livro));

            foreach(var item in carrinho) {
                Console.Write(carrinho.IndexOf(item));
                Console.WriteLine($" {item.Nome} {item.Preco}");
            }
        }
    }
}
