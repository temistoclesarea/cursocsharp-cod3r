# Curso CSharp - Cod3r - Aperfeiçoamento

### Compilar usando Visual Studio Code
dotnet run

### Compilar o projeto estando em uma Gerenciador de Solução
dotnet run --project CursoCSharp

### Gerenciador de Soluções no Visual Studio Code - DOC
https://docs.microsoft.com/pt-br/dotnet/core/tutorials/library-with-visual-studio-code


### Criar Estrutura de Gerenciador de Solução no Visual Studio Code

#### Criar um template Solution File
dotnet new sln

#### Criar projeto de biblioteca de classes
dotnet new classlib -o NomeBibliotecaLibrary

#### Adicionar a biblioteca a solução criada anteriormente
dotnet sln add NomeBibliotecaLibrary/NomeBibliotecaLibrary.csproj

#### Conferir TargetFramework em .csproj
<TargetFramework>net5.0</TargetFramework> (não funcionou)
<TargetFramework>netstandard2.0</TargetFramework> (esse foi o padrão gerado)

#### Verificar se esta compilado e sem erros
dotnet build

#### Criar um aplicativo de console
dotnet new console -o CursoCSharp

#### Adicionar o projeto de aplicativo a solução
dotnet sln add CursoCSharp/CursoCSharpCod3rUdemy.csproj

#### Adicionar uma referência ao projeto
dotnet add CursoCSharp/CursoCSharpCod3rUdemy.csproj reference NomeBibliotecaLibrary/NomeBibliotecaLibrary.csproj

Fim :)